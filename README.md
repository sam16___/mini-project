Command line to compile the project-

g++ IMT2019521.cpp IMT2019522.cpp main.cpp

Command line to execute the project-

./a.out polygon1.ply output.ply test.txt A B star_field.ascii.ppm output.ascii.ppm

In the command line to execute-

polygon1.ply can be replaced by polygon2.ply

star_field.ascii.ppm can be replaced by snail.ascii.ppm (or) feep.ascii.ppm (or) blackbuck.ascii.ppm (or) sines.ascii.ppm (or) pbmlib.ascii.ppm